import React, { Component } from 'react';
import './styles/App.css';
//components
import { Game } from './components/Game';

class App extends Component {


  render() {
  
    return (
      <div className="App App-title">
        <h1 className={'App-center'}>Juego de Memoria</h1>
        <Game />
      </div>
    );
  }
}

export default App;
